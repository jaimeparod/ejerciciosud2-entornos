package main;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		int[] numeros = new int[2];
		
		for (int i = 0; i < 2; i++) {
			System.out.println("Introduce un numero:");
			numeros[i] = scan.nextInt();
		}
		
		maximo(numeros);
		minimo(numeros);
		potencia(numeros);
		
		scan.close();
		
	}

	private static void potencia(int[] numeros) {
		
		System.out.println(numeros[0]+" elevado a "+numeros[1]+" = "+Math.pow(numeros[0], numeros[1]));
		
	}

	private static void minimo(int[] numeros) {
		
		System.out.println("El menor numero es: "+Math.min(numeros[0], numeros[1]));
		
	}

	private static void maximo(int[] numeros) {
		
		System.out.println("El mayor numero es: "+Math.max(numeros[0], numeros[1]));
				
	}

}
